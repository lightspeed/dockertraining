# Creating a Spring Boot Application Docker Image  Using Fabric8

Fabric 8 provides a Maven plugin that can be used to build a Docker image for Spring Boot applications, and then deploy them to a Kubernetes or OpenShift cluster, or push the image to a repository.

In this exercise, you will use Fabric8 to build a docker image using a single command, without requiring a dockerfile or any configuration at all.

The Spring project for this lab can be found here: [starter_projects/Fabric8SpringBootProject](starter_projects/Fabric8SpringBootProject).

The solution project for this lab can be found here: [starter_projects/Fabric8SpringBootProject](solution_projects/Fabric8SpringBootProject).

This lab will take approximately 10 minutes to complete.

## Prerequisites
1. You will to have access to an environment with Docker installed and running.
2. You will require Maven to be installed and set up to work on the command line.


##  Part 1 Edit the Pom File
1. If you haven't already, clone this project into a machine that has Docker installed and running.
2. Using your preferred IDE, open the project in your local copy located here: [starter_projects/Fabric8SpringBootProject](starter_projects/Fabric8SpringBootProject). This is a simple Spring Boot project that hosts a simple Web page and REST API.
3. Open **pom.xml** which is in the root of the project.
4. Locate your build section and add in the fabric8 build plugin:
   
        <build>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId> 
            </plugin>
            <plugin>
                <groupId>io.fabric8</groupId>
                <artifactId>fabric8-maven-plugin</artifactId> 
                <version>4.4.1</version>
            </plugin>
            </plugins>
        </build>

This is required to complete the docker image building and running.

That is basically it! So now we can build and run our container

## Part 2: Build and Run the Image

1. At a terminal within the root of your project, run the following Maven command to create the docker image.

        mvn package fabric8:build

 ![Building the Image with Fabric8](img/build-fabric-8.png)

2. To run the image locally, you can check the image name with

        docker images
    
    The image name is based on the project name in the pom file, so if you haven't changed it, it will be called training/springbootexample.

3. Identify the new image, and launch it with

        docker run -d -p 8080:8080 IMAGENAME
    
    So if you haven't changed the name, it will be:

        docker run -d -p 8080:8080 training/springbootexample

4. Finally, launch a browser, and visit the following URL: http://localhost:8080. You should see the following Web page:

![Expected Output](img/cd-webpage.png)



## Part 3 Deploying to OpenShift using Fabric8

When you built your image locally, you may have noticed some warnings in the console about being unable to locate a cluster.

![Fabric8 Warnings](img/fabric8-warnings.png)

This is because we were running Docker locally and there was no Kubernetes or OpenShift cluster to use. You will now test this same project with OpenShift. We can test this out using a Redhat playground. This is an OpenShift environment that will last for 1 hour.

1. In a separate tab, go to the following Web page https://learn.openshift.com/playgrounds/ and start an OpenShift 4.2 playground.
2. Login to the terminal using the command

        oc login -u admin -p admin

3. Now clone this Git repository into that environment. You can retrieve the git clone command from the clone button at the top right of this Web page.

4. Change directory into the solution project that already has the relevant build entry for fabric8.


        cd dockertraining/solution_projects/Fabric8SpringBootProject/

5. Now you can run the maven command to build the image as before, but notice the output this time.

        mvn package fabric8:build

Your build has now been done differently to before since it is using an OpenShift image stream to complete the build process. This happens automatically since fabric8 is now detecting a cluster.

![Fabric8 Build on OpenShift](img/fabric-build-openshift.png)

6. We can also use Fabric8 to launch the image as a container on OpenShift. Run the following command to complete the deployment.

        mvn fabric8:resource fabric8:deploy

Now let's visit the OpenShift Web console to see what has been created.

7. In the **Redhat Playground**, click on the **Console** tab.
8. Login to the console using admin/admin.
9. In the left pane, ensure that the **</>Developer** perspective is selected.
10. Once in the Developer perspective, in the left menu options, select **Topology**.
11. In the Project drop down, select **default** which is the Project where our container went.
12. You will see in the centre of the screen your pod configuration.

![Pod on OpenShift](img/fabric_pod.png)

13. Click on the pod, and you can see various aspects of configuration that was all set up for us automatically through the fabric8 plugin. 
14. Now click on the link icon on the top right of the blue circle, and you will see your deployed application!

![Application Running on OpenShift](img/cds_openshift.png)


Congratulations. You have now used the fabric8 Maven plugin to successfully build a docker image locally, and then you used the same plugin to build and deploy a SpringBoot application to OpenShift with zero configuration!
