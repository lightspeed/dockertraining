# Docker at Scale – Introduction to OpenShift

In this module you have been introduced to OpenShift, which allows you to run Docker containers at scale, and also can facilitate a CI/CD pipeline. Within the context of LightSpeed, much of the OpenShift detail is handled for you, and if you did interact with it, it would be via command line tools in automation scripts. However, in this exercise, to help you visualise and understand what is happening, you will use the OpenShift Web based console.

The exercise is divided into 6 parts:

[Part 1: Log in to the OpenShift portal](#markdown-header-part-1-log-in-to-the-citi-openshift-sandbox-portal)

[Part 2: Create a project](#markdown-header-part-2-create-a-project)

[Part 3: Configure and Build the Project](#markdown-header-part-3-configure-and-build-the-project)

[Part 4: Review the Running Container ](#markdown-header-part-4-review-the-running-container)

[Part 5: Review the Configuration](#markdown-header-part-5-review-the-configuration)

* Build Configuration
* Deployment Configuration

[Part 6: Clean Up](#markdown-header-part-6-clean-up)

## Part 1: Log in to the Citi OpenShift Sandbox portal

**DO NOT ATTEMPT TO DEPLOY ANY CITI APPLICATIONS THROUGH THIS INTERFACE AS IT IS OUTSIDE THE CITI NETWORK**

1. Using Chrome or Firefox, visit <https://openshift.conygre.com:8443>

2. Since the server is using a self signed certificate, you will need to tell your browser that you are happy to proceed. Below is the Firefox warning:

    ![](./media/image1.png)

3. Click **Advanced**, and then click **Accept the Risk and Continue**.

4. At the **OpenShift Origin welcome page**, use your own name as both the username and the password.

    ![](./media/image2.png)

You are now looking at the OpenShift catalog. the benefit of the catalog is that you can select a template for the kind of application that you would like to deploy to OpenShift and the template will have all the common settings required for that kind of application. Feel free to browse and view the available templates in this OpenShift installation.

![](./media/image3.png)

LightSpeed has a set of templates that are Citi approved and that you can use within the bank.

You will now create a project to host your application.

## Part 2: Create a project

1. In the **Open Shift Console**, at the top right corner, locate and then click **Create Project**.

2. We will create a sample NodeJS project, so at the **Name** field, enter nodejs-yourname, where you swap the text yourname with your actual name, eg. nodejs-johndoe.

3. At the **Display Name** field, enter YourName’s First Project.

4. At the **Description** field, enter My first project.

5. Click **Create**. You will see a popup message telling you that your project has been successfully created.

    ![](./media/image4.png)

6. In the **My Projects** right pane you will see that your project has now been created and is in the list of 1 of 1 projects.

    ![](./media/image5.png)

You have now successfully created an OpenShift project, and you will now point it at a Git repository containing an application.

## Part 3: Configure and Build the Project

1. In the right pane, click on **YourName’s First Project**.

2. You will then see a **Getting Started with your Project** page. Click **Browse Catalog**.

3. At the **Browse Catalog** page, click **NodeJS**.

    Note the Sample Repository link, we will be using that project. <https://github.com/openshift/nodejs-ex.git>

    ![](./media/image6.png)

4. At the **NodeJs Information** page, click **Next**.

5. At the **Configuration** page, notice that the project is already set to your project, and for the remaining fields, click **Try Sample Repository**. This will populate the remaining fields for you.

6. Click **Create**.

    ![](./media/image7.png)

    Your project has now been created, and it will be busy building your application and setting it all up.

7. To see what is happening, in the **Results** page, click **Continue to the Project Overview**. Here you will see the project building successfully. Eventually, you will see the message **Build \#1 is complete**.

    ![](./media/image8.png)

So - what just happened? To find out, let’s look at the build configuration.

1. In the left pane, click **Builds**, and then click **Builds**. You will see your build in a list of builds.

2. Under the **Name** column, click the link **nodejs-ex**.

3. In the **nodejs-ex1-1** pane, click the **Configuration** tab. Notice the **Builder Image** property which references **openshift/nodejs:4**. This is a Docker image. *The application was built within a docker container*.

    ![](./media/image9.png)

    This is very important. Not only are Docker containers going to be used to host the application. Docker containers are also used to build the application from the code checked out from Git. How this works will be discussed later.

4. Now let’s see what actually happened by reviewing the logs. Click on the **History** tab, and then click the **\#1** link. Finally, to see the logs, click the **Logs** tab.

    ![](./media/image10.png)

The log output is coming from the container running the build, and you can see that most of it is the downloading of the various NodeJS dependencies. At the end of the logs, you can see a set of Pushed x/10 layers, xx% complete messages. This is where your image for your application has been pushed to a Docker registry.

## Part 4: Review the Running Container

The pushed image has actually been deployed to a running container as well. This will be within a pod. Remember, a pod is an application in OpenShift that comprises of one or more containers.

1. In the **OpenShift Web Console**, in the left pane, click **Overview**. You will see your application listed as a Deployment.

    ![](./media/image11.png)

2. Note also the blue link on the right. This is a link to view the application in the running container. Click on the link to verify that your application works correctly. You will see a Web page something like this:

    ![](./media/image12.png)

This is your running application. Note the URL. It is to the OpenShift server but with a custom subdomain: **nodejs-ex-nodejs-yourname**.openshift.conygre.com.

How was this set up? Let’s return to the console to see the configuration.

3. Return to the **OpenShift Console**. Expand the **Deployment**.

    ![](./media/image13.png)

4. Here you can see the configuration of your application. Note the **Image** that is being used. That is the one that was created by the build step.

5. Note the Ports 8080/TCP. This is the port that the container is running on. We can see the configuration for this now. In the left pane, click **Applications** and then click **Routes**.

6. Click on the **nodejs-ex** route that is shown.

7. Review the configuration. The container is running on port 8080, but it is then exposed through the OpenShift server on the DNS name specified above. You can see the configuration for the external traffic in the bottom right.

## Part 5: Review the Configuration

Within Citi, you have LightSpeed. One of the aims of LightSpeed is to simplify how you would create and configure applications that are deployed on OpenShift. There are many configuration files that are used, and the good news is that with LightSpeed, much of this is handled for you.

We will now though look at some of the configuration files in the Git project we are using, and see how OpenShift is using them to set up the project you just deployed.

### Build Configuration

1. In the **OpenShift Web Console**, click **Builds**, and then click **Builds**.

2. On the right of the screen, click the **Actions** drop down, and then click **Edit YAML**.

3. You can see in this YAML file the reference to the Github repository, and the image that is being used to complete the build.

        source:
            git:
                ref: master
                uri: 'https://github.com/openshift/nodejs-ex.git'
                type: Git
            strategy:
                sourceStrategy:
                    from:
                        kind: ImageStreamTag
                        name: 'nodejs:4'
                        namespace: openshift
            type: Source

    This is how it knew what image was to be used to build the container for our application in part 2.

1. Once you have finished reviewing the configuration, click **Cancel**.

### Deployment Configuration

You will now see the deployment configuration.

1. In the left pane, click **Applications**, and then click **Deployments**.

2. Click on the **nodejs-ex** deployment.

3. Click **Actions** and then click **Edit YAML**.

4. Note the spec section. Here you can see the port configuration that the application runs on port 8080. You can also see a reference to the image that was created in the Build step.

        spec:
            containers:
              - image: >-
                    172.30.1.1:5000/nodejs-yourname/nodejs-ex@sha256:6fae50c0379131e151e64d62fb1986969b387b96d1e8e504f65eb43e07a96bf2
                    imagePullPolicy: Always
                    name: nodejs-ex
                    ports:
                    - containerPort: 8080
                        protocol: TCP

Feel free to review any other parts of the configuration by reviewing
the various sections within the OpenShift interface.

## Part 6: Clean Up

Finally, we can clean up our application and remove it from the OpenShift server.

1.  In the **OpenShift Web Console**, click on the drop down with **YourNameFirstProject** in it, and then click **View All Projects**.

    ![](./media/image14.png)

2. At the right end of your project name, click on the 3 vertical dots and then click **Delete Project**.

3. At the **Are you sure** dialog box, enter your project name and then click **Delete**.

    ![](./media/image15.png)

**Congratulations!** You have just successfully deployed an OpenShift project and seen it running on our OpenShift server.

Everything you completed in this exercise can also be done (and indeed would normally be done) via command line tools, but the benefit of this exercise, is that it gives you an idea of how the tool is actually working.
