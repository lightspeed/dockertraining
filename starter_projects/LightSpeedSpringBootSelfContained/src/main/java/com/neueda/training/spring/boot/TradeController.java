package com.neueda.training.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("trades")
@EnableAutoConfiguration

@EnableSwagger2
@CrossOrigin
public class TradeController {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TradeController.class, args);
    }

    private static Map<Integer, Trade> library;

    static {
        library = new HashMap<Integer, Trade>();
        library.put(1, new Trade(1, "C", "Buy", 53.54, 200));
        library.put(2, new Trade(2, "BMY", "Sell", 56.23, 350));
        library.put(3, new Trade(3, "FDX", "Sell", 135.43, 600));
        library.put(4, new Trade(4, "HPQ", "Buy", 16.02, 800));
        library.put(5, new Trade(5, "GS", "Buy", 205.42, 200));
        library.put(6, new Trade(6, "PEP", "Buy", 132.28, 400));

    }


    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades() {
        return library.values();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Trade getTradeById(@PathVariable("id") int id) {
        return library.get(id);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteTrade(@PathVariable("id") int id) {
        library.remove(id);
    }


    @RequestMapping(method = RequestMethod.POST,
            consumes = "application/json")
    public void addTrade(@RequestBody Trade p) {
        library.put(p.getId(), p);
    }


    @RequestMapping(method = RequestMethod.PUT,
            consumes = "application/json")
    public void updateTrade(@RequestBody Trade p) {
        if (library.containsKey(p.getId())) {
            library.put(p.getId(), p);
        }

    }
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("trades")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Trade Management REST API")
                .description("This is a CRUD API for trades")
                .contact("Nick Todd")
                .build();
    }
}