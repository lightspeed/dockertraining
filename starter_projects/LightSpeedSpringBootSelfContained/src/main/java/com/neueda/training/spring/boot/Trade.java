package com.neueda.training.spring.boot;

public class Trade {

    private int id;
    private String action;
    private Double price;
    private Integer volume;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    private String symbol;



    public Trade(int id, String symbol, String action, Double price, Integer amount) {
        this.id = id;
        this.symbol = symbol;
        this.action = action;
        this.price = price;
        this.volume = amount;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }
}
